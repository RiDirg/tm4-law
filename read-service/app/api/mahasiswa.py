from fastapi import APIRouter, HTTPException

from app.api.model import Mahasiswa, MahasiswaOut
from app.api.db import add_mahasiswa, get_mahasiswa

mahasiswa = APIRouter()

@mahasiswa.get("/")
async def hello_world():
    return {"message": "hello world"}

@mahasiswa.post("/", response_model=Mahasiswa, status_code=201)
async def create_mahasiswa(payload: Mahasiswa):
    await add_mahasiswa(payload)
    return payload.dict()

@mahasiswa.get("/{npm}", response_model=MahasiswaOut, status_code=200)
async def retrieve_mahasiswa(npm: str):
    mhs = await get_mahasiswa(npm)
    if not mhs:
        raise HTTPException(status_code=404, detail="Mahasiswa not found")
    return {
        "status":"OK",
        **mhs
    }
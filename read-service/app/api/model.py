from pydantic import BaseModel

class StatusOut(BaseModel):
    status: str

class Mahasiswa(BaseModel):
    npm: str
    nama: str

class MahasiswaOut(Mahasiswa, StatusOut):
    pass
import os

from fastapi import FastAPI
from pydantic import BaseModel
from sqlalchemy import create_engine

DATABASE_URI = os.getenv("DATABASE_URI")

engine = create_engine(DATABASE_URI)

class Mahasiswa(BaseModel):
    npm: str
    nama: str

async def update_mahasiswa(mahasiswa: Mahasiswa):
    with engine.connect() as con:
        con.execute(f"UPDATE mahasiswa SET nama='{mahasiswa.nama}' WHERE npm='{mahasiswa.npm}'")

app = FastAPI()

@app.post("/api", status_code=200)
async def update_mhs(mahasiswa: Mahasiswa):
    await update_mahasiswa(mahasiswa)
    return {"status":"OK"}
